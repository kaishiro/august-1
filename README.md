# August - Activity one.

### A live example of this project can be found at: https://august-1.netlify.com/

For this task, I was working under the assumption that this tests the modfication of an existing web space.  To that end, I've modified the existing HTML as little as possible, relegating most of changes to CSS for the responsive implementation. 

## Setup

This project uses [Parcel](https://parceljs.org/) for local development and production builds. It's spec'd as a dev dependency so doesn't need to be installed globally. The following should be enough to get the project up and running locally.

```javascript
// Install dependencies
npm i

// Run locally
npm run develop
```
